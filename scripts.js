    
//Global Variables
var img;
var rand;
 
// Blueprint for each card               
function Card(value, suit) // Card function  assigns the value and an image to a card.
{
    this.name = value;
    this.image = value + suit + ".png";


}

function Deck() // Constructor Card Function starts here 
{

    this.cards = [];   //Card Array



    var suits = ['h', 'd', 'c', 's'];  //suit array  
    var values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'K', 'Q', 'J', 'A'];  //value array



    for (i = 0; i < suits.length; i++) //for loop assignes value & suit to 52 cards
    {
        for (u = 0; u < values.length; u++)
        {
            this.cards[this.cards.length] = new Card(values[u], suits[i]);
        }

    }

    this.shuffle = function () //Shuffle Function shuffles the Deck;   
    {
        var newDeck = [];
        for (var i = 0; i < this.cards.length; i++)
        {
            var rand = Math.floor(Math.random() * 52);
            while (newDeck[rand] != undefined) // Makes sure card hasn't been used
            {
                rand = Math.floor(Math.random() * (52));
            }
            newDeck[rand] = this.cards[i]; // stores random card value 
        }
      strDckImg('back.png')
      this.cards = newDeck;
    
    }
   
}

function strDckImg(thisImg)   //function pupulates divContent with images
{
    var img;
    document.getElementById("divContent").innerHTML=""; //removes priviously stored images
    for (var a = 0; a < newDeck.cards.length; a++)
    {
        img = document.createElement("IMG");
        img.src = "imgs/" + thisImg;
        img.id = 'c' + a;                      //assigns ID's 
        document.getElementById('divContent').appendChild(img);

    }

}

/*
function shwrndm() //show random card function starts here
{
	document.getElementById("divContent").innerHTML="";  //Empties the div
	strDckImg('back.png'); 
	var count= 52;
	var rand = Math.floor(Math.random() * index.length);
  count--;
  r = rand;
  Id = c + r = cr;
    
	while(strDckImg('back.png') == "back.png" && count > 0);
    {
    	
        document.getElementById(id).src = cards[r].image; 
        

    }
        
}  */
function revealcards()  //function reveals all images
{   
    document.getElementById("divContent").innerHTML="";  //Empties the div
    var img;
                                  
    for (var a = 0; a < newDeck.cards.length; a++)
    {
        img = document.createElement("IMG");
        img.src = "imgs/" + newDeck.cards[a].image;
        img.id = 'c' + a;                      //assigns ID's 
        document.getElementById('divContent').appendChild(img);

    }

}

var newDeck;
window.onload = function ()
{
    newDeck = new Deck(); 
    strDckImg('back.png');
}
